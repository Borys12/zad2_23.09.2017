public class TVseries {
	String name;
	int season;
		
	public String getName(){
		return name;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public int getSeason(){
		return season;
	}
	
	public void setSeason(int season){
		this.season = season;
	}
}
