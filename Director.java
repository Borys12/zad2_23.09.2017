import java.time.LocalDate;

public class Director {
	String name;
	String biograph;
	LocalDate dayOfBirth;
	
	public String getName(){
	  return name;
	}
 
	public void setName(String name){
	  this.name = name;
	}
		
	public String getBiograph(){
	  return biograph;
	}
  
	public void setBiograph(String biograph){
	  this.biograph = biograph;
	}
	
	public LocalDate getDayOfBirth(){
	 return dayOfBirth;
	}
 
	public void setDayOfBirth(LocalDate dayOfBirth){
	 this.dayOfBirth = dayOfBirth;
	}	
} 

	