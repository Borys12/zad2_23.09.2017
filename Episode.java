import java.time.LocalDate;

public class Episode {
	String name;
	double  episodeLength;
	int episodeNumber;
	LocalDate pilotAirDate;

	public String getName(){
		return name;
	}
	
	public void setName(String name){
		this.name = name;		
	}
	
	public double getEpisodeLength(){
		return episodeLength;
	}
	
	public void setEpisodeLength(double episodeLength){
		this.episodeLength = episodeLength;
	}
	
	public int geEpisodeNumber(){
		return episodeNumber;
	}
	 
	public void setEpisodeNumber(int episodeNumber){
		this.episodeNumber = episodeNumber;
	}
	
	public LocalDate getPilotAirDate(){
		return pilotAirDate;
	}
	
	
	public void setPilotAirDate(LocalDate pilotAirDate){
		this.pilotAirDate = pilotAirDate;
	}		
}

