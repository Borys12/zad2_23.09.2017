import java.time.LocalDate;

public class Actor {
	String name;
	String biograph;
	LocalDate dateOfBirth;

	public String getName(){
		return name;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public String getBiograph(){
		return biograph;
	}
	
	public void setBiography(String biograph){
		this.biograph = biograph;
	}
	
	public LocalDate getDateOfBirth(){
		return dateOfBirth;
		
	}
	public void setDateOfBirth(LocalDate dateOfBirth){
		this.dateOfBirth = dateOfBirth;
	}
}
